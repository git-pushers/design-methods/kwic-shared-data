import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;


public class Main {

    // Shared data
    static ArrayList<line> index;

    static class line {
        int page;
        int line;
        int word; // Указатель на слово

        String[] words;
        String origin;

        public line(int page, int line, int word, String[] words, String origin) {
            this.page = page;
            this.line = line;
            this.word = word;
            this.words = words;
            this.origin = origin;
        }
    }

    public static void main(String[] args) throws IOException {
        if (args.length == 0) {
            System.out.println("invalid file name");
            return;
        }
        String filename = args[0];


        index = readLines(filename);    // 1 - Чтение
        shiftIndex();                   // 2 - Сдвиг индекса
        sortIndex();                    // 3 - Сортировка индекса
        printIndex();                   // 4 - Печать индеса


    }

    private static void printIndex() {
        String format = "%-80s %-30s %-20s %-20s\n";
        System.out.format(format, "Context", "Key", "Page", "Line");
        for (line l : index) {
            System.out.format(format, l.origin, l.words[l.word - 1], l.page, l.line);
        }
    }

    private static void sortIndex() {
        index.sort(Comparator.comparing((s) -> s.words[s.word - 1]));
    }

    private static void shiftIndex() {
        ArrayList<line> shiftedIndex = new ArrayList<line>();
        for (line l : index) {
            for (int i = 0; i < l.words.length; i++) {
                shiftedIndex.add(new line(l.page, l.line, i + 1, l.words, l.origin));
            }
        }
        index = shiftedIndex;
    }

    private static ArrayList<line> readLines(String filename) throws IOException {
        index = new ArrayList<line>();
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            int pageNum = 1;
            int lineNum = 1;

            for (String line; (line = br.readLine()) != null; ) {
                if (line.equals("#")) {
                    pageNum++;
                    lineNum = 1;
                    continue;
                }
                index.add(new line(pageNum, lineNum, 1, line.split(" "), line));
                lineNum++;
            }
        }
        return index;
    }

}


